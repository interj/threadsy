#include <iostream>
#include <thread>
#include <string>
#include <vector>

void hello(std::string message, int number)
{
    std::cout << "Hello, world!" << std::endl;
    std::cout << "Args passed: " + message + ", " << number << std::endl;
}

int increase(int& num)
{
    std::cout << num << std::endl;
    return ++num;
}

struct F
{
    void operator()(int& num)
    {
        std::cout << (num += 10) << std::endl;
    }
};

class Buff
{
    std::vector<int> buffer;
public:
    void push(const std::vector<int> items)
    {
        buffer.assign(items.begin(), items.end());
    }

    void print()
    {
        std::cout << "buffered: " << std::endl;
        for(const auto& item : buffer)
        {
            std::cout << item << ", ";
        }
        std::cout << std::endl;
    }
};

int main(int argc, char **argv)
{
    std::thread th(hello, "something, something", 15);
    int val = 5;
    std::thread refThread(increase, std::ref(val));
    F f;
    std::thread refThread2(f, std::ref(val));
    std::thread lThread([](int i){ std::cout << "lambda with " << i << std::endl; }, 19);
    Buff b;
    std::vector<int> v{1, 5, 4, 5, 4, 5, 2, 45,8 ,887, 85, 56};
    std::thread buffThread(&Buff::push, &b, std::cref(v));

    th.join();
    refThread.join();
    refThread2.join();
    lThread.join();
    buffThread.join();

    b.print();
    std::cout << val << std::endl;
    return 0;
}
